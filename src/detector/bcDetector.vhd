library ieee;
use ieee.std_logic_1164.all;

entity bcDetector is
	generic(ADDR_WIDTH : natural := 100
	);
	port(
		inicia, clk, rst                              : in  std_logic;
		imagemCarregada, gxPronto, gyPronto, gPronto  : in  std_logic;
		carregaImagem, calculaGx, calculaGy, calculaG : out std_logic;
		bordaDetectada                                : out std_logic
	);
end entity bcDetector;

architecture comportamento of bcDetector is
	type estado_type is (ocioso, carregandoImagem, calculandoGx, calculandoGy, calculandoG, finalizado); -- Estado mostrarResultado
	signal estado         : estado_type := ocioso;
	signal gxFoiConcluido : boolean     := false;
	signal gyFoiConcluido : boolean     := false;

	shared variable contadorDeClk : natural := 0;

begin
	process(clk, rst)
	begin
		if (rst = '1') then
			estado <= ocioso;
		elsif (rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= carregandoImagem;
					else
						estado <= ocioso;
					end if;
				when carregandoImagem =>
					if (imagemCarregada = '1') then
						estado <= calculandoGx;
					else
						estado <= carregandoImagem;
					end if;
				when calculandoGx =>
					if (gxPronto = '1') then
						estado <= calculandoGy;
					else
						estado <= calculandoGx;
					end if;
				when calculandoGy =>
					if (gyPronto = '1') then
						estado <= calculandoG;
					else
						estado <= calculandoGy;
					end if;
				when calculandoG =>
					if (gPronto = '1') then
						estado <= finalizado;
					else
						estado <= calculandoG;
					end if;
				when finalizado =>
					if (contadorDeClk = 3) then
						estado <= ocioso;
					else
						estado <= finalizado;
					end if;
			end case;
		end if;
	end process;

	process(clk)
	begin
		if (rising_edge(clk)) then
			if (estado = finalizado) then
				contadorDeClk := contadorDeClk + 1;
			end if;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				carregaImagem  <= '0';
				calculaGx      <= '0';
				calculaGy      <= '0';
				calculaG       <= '0';
				bordaDetectada <= '0';
			when carregandoImagem =>
				carregaImagem  <= '1';
				calculaGx      <= '0';
				calculaGy      <= '0';
				calculaG       <= '0';
				bordaDetectada <= '0';
			when calculandoGx =>
				carregaImagem  <= '0';
				calculaGx      <= '1';
				calculaGy      <= '0';
				calculaG       <= '0';
				bordaDetectada <= '0';
			when calculandoGy =>
				carregaImagem  <= '0';
				calculaGx      <= '0';
				calculaGy      <= '1';
				calculaG       <= '0';
				bordaDetectada <= '0';
			when calculandoG =>
				carregaImagem  <= '0';
				calculaGx      <= '0';
				calculaGy      <= '0';
				calculaG       <= '1';
				bordaDetectada <= '0';
			when finalizado =>
				carregaImagem  <= '0';
				calculaGx      <= '0';
				calculaGy      <= '0';
				calculaG       <= '0';
				bordaDetectada <= '1';
		end case;
	end process;

end architecture comportamento;
	