library ieee;
use ieee.std_logic_1164.all;

entity boDetector is
	generic(DATA_WIDTH : natural := 8;
		    ADDR_WIDTH : natural := 100
	);
	port (
		clk,rst : in std_logic;
		imagem                             : in  std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity boDetector;

architecture comportamente of boDetector is
	
	component carregadorDeImagem
		generic(DATA_WIDTH             : natural := 10;
			    ADDR_WIDTH_IMG_NATURAL : natural := 100);
		port(imagemIN                     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 escreve, carrega, reset, clk : in  std_logic;
			 endereco                     : in  natural range 0 to ADDR_WIDTH_IMG_NATURAL - 1;
			 imagemOUT                    : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			 iPronta                      : out std_logic);
	end component carregadorDeImagem;
	
	com
	
begin
	
end architecture comportamente;
