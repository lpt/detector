library ieee;
use ieee.std_logic_1164.all;

entity bcG is
	generic(ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, reset            : in  std_logic;
		inicia, calculoPronto : in  std_logic;
		calculaG              : out std_logic;
		escreveMemG           : out std_logic;
		endereco              : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		gPronto               : out std_logic
	);
end entity bcG;

architecture comportamento of bcG is
	type estado_type is (ocioso, calcula_G, finalizado);
	signal estado : estado_type := ocioso;

	shared variable contadorDeClk : natural := 0;
	shared variable contador      : natural := 0;
	
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			estado <= ocioso;
		elsif(rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= calcula_G;
					else
						estado <= ocioso;
					end if;
				when calcula_G =>
					if (calculoPronto = '1') then
						estado <= finalizado;
					else
						estado <= calcula_G;
					end if;
				when finalizado =>
					if (contadorDeClk = 3) then
						estado <= ocioso;
					else
						estado <= finalizado;
					end if;
			end case;
		end if;
	end process;

	process(clk)
	begin
		if (rising_edge(clk)) then
			if (estado = calcula_G) then
				endereco <= contador;
				contador := contador + 1;
			elsif (estado = finalizado) then
				contadorDeClk := contadorDeClk + 1;
			end if;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				calculaG      <= '0';
				escreveMemG   <= '0';
				gPronto       <= '0';
			when calcula_G =>
				calculaG    <= '1';
				escreveMemG <= '1';
				gPronto     <= '0';
			when finalizado =>
				calculaG    <= '0';
				escreveMemG <= '0';
				gPronto     <= '1';
		end case;
	end process;
end architecture comportamento;
