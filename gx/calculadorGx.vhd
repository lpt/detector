library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_signed.all;

entity calculadorGx is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk                                             : in  std_logic;
		imagem                                          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		carregaImagem, calculaGx, escreveNaRam          : in  std_logic;
		carrega                                         : out std_logic;
		inicia, carregado, calculado, escritaFinalizada : out std_logic;
		enderecoEscrita, enderecoLeitura                : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		escreve                                         : out std_logic;
		resultado                                       : out std_logic_vector(7 downto 0)
	);
end entity calculadorGx;

architecture comportamento of calculadorGx is
	type matrix is array (0 to DATA_WIDTH - 1, 0 to DATA_WIDTH - 1) of integer;
	type linhas_de_output is array (0 to DATA_WIDTH - 1) of integer;
	type kernel is array (0 to 2, 0 to 2) of integer;
	type pilhaDePixels is array (0 to ADDR_WIDTH_NATURAL - 1) of std_logic_vector(7 downto 0);

	signal matrix_imagem : matrix;

	function convolucaoSimples(linha, coluna : natural) return integer is
		variable resultado : integer := 0;
		variable mascara   : kernel  := ((1, 0, -1), (2, 0, -2), (1, 0, -1));

	begin
		for i in 0 to 2 loop
			for j in 0 to 2 loop
				-- Resolver index out of bounds 
				-- resultado := resultado + matrix_imagem(linha + i, coluna + j) * mascara(i, j);
				resultado := resultado + matrix_imagem(linha, coluna) * mascara(i, j);
			end loop;
		end loop;

		return resultado;
	end function convolucaoSimples;

	function convolucao2D(linha : natural) return linhas_de_output is
		variable linha_matriz : linhas_de_output;

	begin
		for coluna in 0 to DATA_WIDTH - 1 loop
			linha_matriz(coluna) := convolucaoSimples(linha, coluna);
		end loop;

		return linha_matriz;
	end function convolucao2D;

begin
	process(clk)
		variable contador                    : natural := 0;
		variable contador_coluna             : natural := 0;
		variable contador_linha              : natural := 0;
		variable linhaParaConverterParaPixel : linhas_de_output;
		variable pixels                      : pilhaDePixels;
	begin
		if (rising_edge) then
			if (carregaImagem = '0' AND calculaGx = '0' AND escreveNaRam = '0') then
				carrega         <= '0';
				enderecoLeitura <= 0;
				resultado       <= (others => '0');
			elsif (carregaImagem = '1' AND calculaGx = '0' AND escreveNaRam = '0') then
				enderecoLeitura                                <= contador_linha * 10 + contador_coluna;
				matrix_imagem(contador_linha, contador_coluna) <= CONV_INTEGER(imagem);
				contador_coluna                                := contador_coluna + 1;
				if (contador_coluna >= DATA_WIDTH) then
					contador_coluna := 0;
					contador_linha  := contador_linha + 1;
				end if;
				if (contador_linha >= DATA_WIDTH) then
					contador_linha  := 0;
					contador_coluna := 0;
					carregado       <= true;
				end if;
			elsif (carregaImagem = '0' AND calculaGx = '1' AND escreveNaRam = '0') then
				escreve                     <= '0';
				linhaParaConverterParaPixel := convolucao2D(contador);
				for i in 0 to DATA_WIDTH - 1 loop
					pixels(contador) := std_logic_vector(to_signed(linhaParaConverterParaPixel(i), 8));
				end loop;
				contador := contador + 1;
				if (contador = DATA_WIDTH) then
					calculado <= true;
					contador  := 0;
				end if;
			elsif (carregaImagem = '0' AND calculaGx = '0' AND escreveNaRam = '1') then
				escreve         <= '1';
				enderecoEscrita <= contador;
				resultado       <= pixels(contador);
				contador        := contador + 1;
				if (contador = ADDR_WIDTH_NATURAL) then
					escritaFinalizada <= true;
					contador          := 0;
				end if;
			end if;
		end if;
	end process;

end architecture comportamento;
