Sistema Digital Detector, para Detectar Borda de Imagem
----------

Projeto DETECÇÃO DE BORDA DE IMAGEM. O nosso objetivo é descrever em VHDL blocos de hardware capazes de detectar borda de uma imagem de entrada no nosso sistema e a saída é uma imagem processada. Basicamente a detecção de borda vem da interpretação geométrica da imagem, pois é uma técnica de processamento de imagem para determinar pontos em que a intensidade luminosa muda repentinamente, também conhecido como alto contraste. Essas mudanças em imagens normalmente refletem eventos importantes no cenário, como descontinuação da profundidade (transição entre o objeto e o fundo), descontinuação da superfície, mudança das propriedades do material ou variações na iluminação da cena. Para uma captação de variação entre um pixel x e seu pixel vizinho $x + 1$, e utilizando uma relação matemática construída em cima das máscaras (i.e: Operador Sobel, Operador Prewitt, dentre outros operadores) que são peça chave para a convolução de uma matriz de pixels, onde cada pixel é representado por uma faixa de 8 bits $00000000$ significa cor preta e $11111111$ cor branca, qualquer coisa entre isso são tons de cinza. Nosso sistema digital é síncrono com reset assíncrono. Para esse projeto o leitor poderá notar que estamos pensando num projeto padronizado, separando em blocos e generalizando esses mesmos de maneira que se possa reutilizá-los. Existe um documento para esse projeto para caso você queira mais informações.

Todo
----------

Para finalizar o projeto, falta concretizar e ajustar o tempo de clock para que a imagem que entra no bloco seja devidamente armazenada, então consecutivamente falta ajustar o tempo de clock para as operações com esse imagem.
