library ieee;
use ieee.std_logic_1164.all;

entity carregadorDeImagem is
	generic(DATA_WIDTH             : natural := 10;
		    ADDR_WIDTH_IMG_NATURAL : natural := 100
	);
	port(
		imagemIN                     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		escreve, carrega, reset, clk : in  std_logic;
		endereco                     : in  natural range 0 to ADDR_WIDTH_IMG_NATURAL - 1;
		imagemOUT                    : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		iPronta                      : out std_logic
	);
end entity;

architecture comportamento of carregadorDeImagem is
	component memoriaRam is
		generic(
			DATA_WIDTH         : natural := 8;
			ADDR_WIDTH_NATURAL : natural := 6
		);

		port(
			clk  : in  std_logic;
			addr : in  natural range 0 to ADDR_WIDTH_NATURAL - 1;
			data : in  std_logic_vector((DATA_WIDTH - 1) downto 0);
			we   : in  std_logic := '1';
			q    : out std_logic_vector((DATA_WIDTH - 1) downto 0)
		);
	end component;

	signal img        : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal addr_reg : natural range 0 to ADDR_WIDTH_IMG_NATURAL - 1;
	signal contador : natural := 0;
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			iPronta  <= '0';
			contador <= 0;

		elsif (rising_edge(clk)) then
			if (escreve = '1' AND carrega = '0') then
				if (contador = ADDR_WIDTH_IMG_NATURAL) then
					iPronta  <= '1';
					contador <= 0;
				else
					img      <= imagemIN;
					addr_reg <= contador;
					contador <= contador + 1;
					iPronta  <= '0';
				end if;
			elsif (escreve = '0' AND carrega = '1') then
				addr_reg <= endereco;
			end if;
		end if;
	end process;

	mRam : memoriaRam GENERIC MAP(DATA_WIDTH => DATA_WIDTH, ADDR_WIDTH_NATURAL => ADDR_WIDTH_IMG_NATURAL) PORT MAP(clk, addr_reg, img, escreve, imagemOUT);

end architecture;