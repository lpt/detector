library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity calculadorG is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, calcula     : in  std_logic;
		pixelGx, pixelGy : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		contador         : in  natural range 0 to ADDR_WIDTH_NATURAL - 1;
		pronto           : out std_logic;
		resultado        : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity calculadorG;

architecture comportamento of calculadorG is
begin
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (calcula = '1') then
				if (contador = 99) then
					pronto <= '1';
				end if;
		-- ARRUMAR 
--Error (10327): VHDL error at calculadorG.vhd(26): can't determine definition of operator ""+"" -- found 0 possible definitions
-- resultado <= abs(signed(pixelGx)) + abs(signed(pixelGy));
				resultado <= (others => '0');
			end if;
		end if;
	end process;

end architecture comportamento;

