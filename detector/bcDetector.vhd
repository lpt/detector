library ieee;
use ieee.std_logic_1164.all;

entity bcDetector is
	generic(DATA_WIDTH : natural := 8;
		    ADDR_WIDTH : natural := 100
	);
	port(
		inicia, clk, rst                   : in  std_logic;
		imagemCarregada, calculoFinalizado : in  std_logic;
		carregaImagem, calcula             : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		resultado                          : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity bcDetector;

architecture comportamento of bcDetector is
	type estado_type is (ocioso, carregandoImagem, calculando); -- Estado mostrarResultado
	signal estado : estado_type := ocioso;
begin
	process(clk, rst)
	begin
		if (rst = '1') then
			estado <= ocioso;
		elsif (rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= carregandoImagem;
					end if;
				when carregandoImagem =>
					if (imagemCarregada = '1') then
						estado <= calculando;
					end if;
				when calculando =>
					if (calculoFinalizado = '1') then
						estado <= ocioso;
					end if;
			end case;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				carregaImagem <= '0';
				calcula       <= '0';
			when carregandoImagem =>
				carregaImagem <= '1';
				calcula       <= '0';
			when calculando =>
				carregaImagem <= '0';
				calcula       <= '1';
		end case;
	end process;

end architecture comportamento;
	