library ieee;
use ieee.std_logic_1164.all;

entity bcG is
	port(
		clk, reset            : in  std_logic;
		inicia, calculoPronto : in  std_logic;
		calculaG              : out std_logic
	);
end entity bcG;

architecture comportamento of bcG is
	type estado_type is (ocioso, calculaG);
	signal estado : estado_type;

begin
	process(clk, reset)
	begin
		if (reset = '1') then
			estado <= ocioso;
		end if;
		if (rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= calculaG;
					else
						estado <= ocioso;
					end if;
				when calculaG =>
					if (calculoPronto = '1') then
						estado <= ocioso;
					else
						estado <= calculaG;
					end if;
			end case;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				calculaG <= 0;
			when calculaG =>
				calculaG <= 1;
		end case;
	end process;
end architecture comportamento;
