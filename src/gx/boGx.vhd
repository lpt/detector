library ieee;
use ieee.std_logic_1164.all;

entity boGx is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk                                             : in  std_logic;
		imagem                                          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		carregaImagem, calculaGx, escreveNaRam          : in  std_logic;
		carregado, calculado, escritaFinalizada : out std_logic;
		carrega                                         : out std_logic;
		enderecoEscrita, enderecoLeitura                : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		resultado                                       : out std_logic_vector(7 downto 0)
	);
end entity boGx;

architecture comportamento of boGx is
	component calculadorGx
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk                                             : in  std_logic;
			imagem                                          : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			carregaImagem, calculaGx, escreveNaRam          : in  std_logic;
			carrega                                         : out std_logic;
			carregado, calculado, escritaFinalizada : out std_logic;
			enderecoEscrita, enderecoLeitura                : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			resultado                                       : out std_logic_vector(7 downto 0)
		);
	end component calculadorGx;

begin
	calcGx : calculadorGx
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk               => clk,
			     imagem            => imagem,
			     carregaImagem     => carregaImagem,
			     calculaGx         => calculaGx,
			     escreveNaRam      => escreveNaRam,
			     carrega           => carrega,
			     carregado         => carregado,
			     calculado         => calculado,
			     escritaFinalizada => escritaFinalizada,
			     enderecoEscrita   => enderecoEscrita,
			     enderecoLeitura   => enderecoLeitura,
			     resultado         => resultado);

end architecture comportamento;
