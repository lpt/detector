library ieee;
use ieee.std_logic_1164.all;

entity bcGy is
	port(
		clk, reset                                      : in  std_logic;
		inicia, carregado, calculado, escritaFinalizada : in  std_logic;
		carregaImagem, calculaGy, escreveNaRam: out std_logic;
		GyPronto: out std_logic;
		escreve: out std_logic
	);
end entity bcGy;

architecture comportamento of bcGy is
	type estado_type is (ocioso, carregandoImagem, calculando, escrevendoNaRam,finalizado);
	signal estado : estado_type  := ocioso;

begin
	process(clk)
	begin
		if (rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= carregandoImagem;
					else
						estado <= ocioso;
					end if;
				when carregandoImagem =>
					if (carregado = '1') then
						estado <= calculando;
					else
						estado <= carregandoImagem;
					end if;
				when calculando =>
					if (calculado = '1') then
						estado <= escrevendoNaRam;
					else
						estado <= calculando;
					end if;
				when escrevendoNaRam =>
					if (escritaFinalizada = '1') then
						estado <= finalizado;
					else
						estado <= escrevendoNaRam;
					end if;
				when finalizado =>
					estado <= ocioso;
			end case;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				carregaImagem <= '0';
				calculaGy     <= '0';
				escreveNaRam  <= '0';
				escreve <= '0';
			when carregandoImagem =>
				carregaImagem <= '1';
				calculaGy     <= '0';
				escreveNaRam  <= '0';
				escreve <= '0';
				GyPronto  <= '0';
			when calculando =>
				carregaImagem <= '0';
				calculaGy     <= '1';
				escreveNaRam  <= '0';
				escreve <= '0';
				GyPronto  <= '0';
			when escrevendoNaRam =>
				carregaImagem <= '0';
				calculaGy     <= '0';
				escreveNaRam  <= '1';
				escreve <= '1';
				GyPronto  <= '0';
			when finalizado =>
				carregaImagem <= '0';
				calculaGy     <= '0';
				escreveNaRam  <= '0';
				escreve <= '0';
				GyPronto  <= '1';
		end case;
	end process;
end architecture comportamento;

