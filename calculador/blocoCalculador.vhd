library ieee;
use ieee.std_logic_1164.all;

entity blocoCalculador is
	port (
		clk,rst : in std_logic
		imagem                                  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
	);
end entity blocoCalculador;
