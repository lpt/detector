library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity calculadorG is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, calcula                                           : in  std_logic;
		pixelGx, pixelGy                                       : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		calculoPronto, escreveG                                : out std_logic;
		enderecoLeituraGx, enderecoLeituraGy, enderecoEscritaG : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		resultado                                              : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity calculadorG;

architecture comportamento of calculadorG is
	variable contador : natural := 0;
begin
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (calcula = '1') then
				if (contador = 100) then
					calculoPronto <= '1';
				else
					enderecoLeituraGx <= contador;
					enderecoLeituraGy <= contador;
					enderecoEscritaG  <= contador;
					escreveG          <= '1';
					resultado         <= abs (signed(pixelGx)) + abs (signed(pixelGy));
				end if;
			end if;
		end if;
	end process;

end architecture comportamento;

