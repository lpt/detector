library ieee;
use ieee.std_logic_1164.all;

entity boG is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, calcula                                           : in  std_logic;
		pixelGx, pixelGy                                       : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		calculoPronto,escreveG                                          : out std_logic;
		enderecoLeituraGx, enderecoLeituraGy, enderecoEscritaG : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		resultado                                              : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity boG;

architecture comportamento of boG is
	component calculadorG
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(clk, calcula                                           : in  std_logic;
			 pixelGx, pixelGy                                       : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 calculoPronto, escreveG                                : out std_logic;
			 enderecoLeituraGx, enderecoLeituraGy, enderecoEscritaG : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 resultado                                              : out std_logic_vector(DATA_WIDTH - 1 downto 0));
	end component calculadorG;
begin
	calculaG : calculadorG
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk               => clk,
			     calcula           => calcula,
			     pixelGx           => pixelGx,
			     pixelGy           => pixelGy,
			     calculoPronto     => calculoPronto,
			     escreveG  => escreveG,
			     enderecoLeituraGx => enderecoLeituraGx,
			     enderecoLeituraGy => enderecoLeituraGy,
			     enderecoEscritaG  => enderecoEscritaG,
			     resultado         => resultado);
end architecture comportamento;
