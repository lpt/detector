library ieee;
use ieee.std_logic_1164.all;

entity boCalculador is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, rst                                : in  std_logic;
		iniciaGx, iniciaGy, iniciaG             : in  std_logic;
		imagem                                  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		carrega                                 : out std_logic;
		gxPronto, gyPronto, gPronto             : out std_logic;
		escreveG                                : out std_logic;
		enderecoLeituraImagem, enderecoEscritaG : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		resultado                               : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity boCalculador;

architecture comportamento of boCalculador is
	component blocoCalculaGx
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(clk, rst                         : in  std_logic;
			 imagem                           : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 inicia                           : in  std_logic;
			 enderecoEscrita, enderecoLeitura : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 carrega, escreve                 : out std_logic;
			 resultado                        : out std_logic_vector(7 downto 0));
	end component blocoCalculaGx;

	component blocoCalculaGy
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(clk, rst                         : in  std_logic;
			 imagem                           : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 inicia                           : in  std_logic;
			 enderecoEscrita, enderecoLeitura : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 carrega, escreve                 : out std_logic;
			 resultado                        : out std_logic_vector(7 downto 0));
	end component blocoCalculaGy;

	component blocoCalculaG
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(clk, rst                                               : in  std_logic;
			 inicia                                                 : in  std_logic;
			 pixelGx, pixelGy                                       : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 calculoPronto, escreveG                                : out std_logic;
			 enderecoLeituraGx, enderecoLeituraGy, enderecoEscritaG : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 resultado                                              : out std_logic_vector(DATA_WIDTH - 1 downto 0));
	end component blocoCalculaG;

	component memoriaRam
		generic(DATA_WIDTH         : natural := 8;
			    ADDR_WIDTH_NATURAL : natural := 6);
		port(clk  : in  std_logic;
			 addr : in  natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 data : in  std_logic_vector((DATA_WIDTH - 1) downto 0);
			 we   : in  std_logic := '1';
			 q    : out std_logic_vector((DATA_WIDTH - 1) downto 0));
	end component memoriaRam;

	signal escreveGx, escreveGy                             : std_logic;
	signal enderecoEscritaMemGx, enderecoEscritaMemGy       : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoLeituraGx, enderecoLeituraGy             : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoMemGx, enderecoMemGy, enderecoMemG       : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal resultadoGx, resultadoGy                         : std_logic_vector(7 downto 0);
	signal enderecoLeituraImagemGx, enderecoLeituraImagemGy : natural range 0 to ADDR_WIDTH_NATURAL - 1;

begin
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (iniciaGx = '1' and iniciaGy = '0' and iniciaG = '0') then
				enderecoMemGx         <= enderecoEscritaMemGx;
				enderecoLeituraImagem <= enderecoLeituraImagemGx;
			elsif (iniciaGx = '0' and iniciaGy = '1' and iniciaG = '0') then
				enderecoMemGy         <= enderecoEscritaMemGy;
				enderecoLeituraImagem <= enderecoLeituraImagemGy;
			elsif (iniciaGx = '0' and iniciaGy = '0' and iniciaG = '1') then
				enderecoMemGx <= enderecoLeituraGx;
				enderecoMemGy <= enderecoLeituraGy;
			end if;
		end if;
	end process;

	memGx : memoriaRam
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk  => clk,
			     addr => enderecoMemGx,
			     data => resultado,
			     we   => escreveGx,
			     q    => gx);
	gx : blocoCalculaGx
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk             => clk,
			     rst             => rst,
			     imagem          => imagem,
			     inicia          => iniciaGx,
			     enderecoEscrita => enderecoEscritaMemGx,
			     enderecoLeitura => enderecoLeituraImagemGx,
			     carrega         => carrega,
			     escreve         => escreveGx,
			     resultado       => resultadoGx);

	memGy : memoriaRam
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk  => clk,
			     addr => enderecoMemGy,
			     data => resultadoGy,
			     we   => escreveGy,
			     q    => gy);
	gy : component blocoCalculaGy
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk             => clk,
			     rst             => rst,
			     imagem          => imagem,
			     inicia          => iniciaGy,
			     enderecoEscrita => enderecoEscritaMemGy,
			     enderecoLeitura => enderecoLeituraImagemGy,
			     carrega         => carrega,
			     escreve         => escreveGy,
			     resultado       => resultadoGy);
	g : component blocoCalculaG
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk               => clk,
			     rst               => rst,
			     inicia            => iniciaG,
			     pixelGx           => gx,
			     pixelGy           => gy,
			     calculoPronto     => gPronto,
			     escreveG          => escreveG,
			     enderecoLeituraGx => enderecoLeituraGx,
			     enderecoLeituraGy => enderecoLeituraGy,
			     enderecoEscritaG  => enderecoEscritaG,
			     resultado         => resultado);

end architecture comportamento;
