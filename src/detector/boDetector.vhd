library ieee;
use ieee.std_logic_1164.all;

entity boDetector is
	generic(DATA_WIDTH         : natural := 8;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		imagem                                        : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		clk, rst                                      : in  std_logic;
		carregaImagem, calculaGx, calculaGy, calculaG : in std_logic;
		imagemCarregada, gxPronto, gyPronto, gPronto  : out std_logic;
		imagemComBordaDetectada : out std_logic_vector(8 - 1 downto 0)
	);
end entity boDetector;

architecture comportamente of boDetector is
	component carregadorDeImagem is
		generic(DATA_WIDTH             : natural := 8;
			    ADDR_WIDTH_IMG_NATURAL : natural := 100
		);
		port(
			imagemIN                     : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			escreve, carrega, reset, clk : in  std_logic;
			endereco                     : in  natural range 0 to ADDR_WIDTH_IMG_NATURAL - 1;
			imagemOUT                    : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			iPronta                      : out std_logic
		);
	end component;

	component memoriaRam
		generic(DATA_WIDTH         : natural := 8;
			    ADDR_WIDTH_NATURAL : natural := 6);
		port(clk  : in  std_logic;
			 addr : in  natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 data : in  std_logic_vector((DATA_WIDTH - 1) downto 0);
			 we   : in  std_logic := '1';
			 q    : out std_logic_vector((DATA_WIDTH - 1) downto 0));
	end component memoriaRam;

	component blocoCalculaGx
		generic(DATA_WIDTH         : natural := 8;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(clk, rst                         : in  std_logic;
			 imagem                           : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			 inicia                           : in  std_logic;
			 enderecoEscrita, enderecoLeitura : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			 carrega, escreve                 : out std_logic;
			 gxPronto                         : out std_logic;
			 resultado                        : out std_logic_vector(7 downto 0));
	end component blocoCalculaGx;
	component blocoCalculaGy
		generic(DATA_WIDTH         : natural := 8;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk, rst                         : in  std_logic;
			imagem                           : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			inicia                           : in  std_logic;
			enderecoEscrita, enderecoLeitura : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			carrega, escreve                 : out std_logic;
			gyPronto                         : out std_logic;
			resultado                        : out std_logic_vector(7 downto 0));
	end component blocoCalculaGy;
	component blocoCalculaG
		generic(DATA_WIDTH         : natural := 8;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk, rst         : in  std_logic;
			inicia           : in  std_logic;
			pixelGx, pixelGy : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			escreveMemG      : out std_logic;
			endereco         : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			gPronto          : out std_logic;
			resultado        : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component blocoCalculaG;

	-- MEMORIA GX
	signal resultadoGx, gx                 : std_logic_vector(7 downto 0);
	signal escreveMemGx                    : std_logic;
	signal enderecoMemGx                   : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoEscritaMemGxPeloBlocoGx : natural range 0 to ADDR_WIDTH_NATURAL - 1;

	-- MEMORIA GY
	signal resultadoGy, gy                 : std_logic_vector(7 downto 0);
	signal escreveMemGy                    : std_logic;
	signal enderecoMemGy                   : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoEscritaMemGyPeloBlocoGy : natural range 0 to ADDR_WIDTH_NATURAL - 1;

	-- MEMORIA G
	signal g,resultadoG	: std_logic_vector(7 downto 0);
	signal escreveMemG            : std_logic;
	signal enderecoMemG           : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoMemGPeloBlocoG : natural range 0 to ADDR_WIDTH_NATURAL - 1;

	-- IMAGEM
	signal carregaImagemReferencia,carregaImagemReferenciaDoGx,carregaImagemReferenciaDoGy : std_logic;
	signal imagemReferencia        : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal enderecoImagem          : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoImagemParaGx    : natural range 0 to ADDR_WIDTH_NATURAL - 1;
	signal enderecoImagemParaGy    : natural range 0 to ADDR_WIDTH_NATURAL - 1;

begin
	process(clk)
	begin
		if (calculaGx = '1' AND calculaGy = '0') then
			enderecoMemGx  <= enderecoEscritaMemGxPeloBlocoGx;
			enderecoImagem <= enderecoImagemParaGx;
			carregaImagemReferencia <= carregaImagemReferenciaDoGx;
		else
			enderecoMemGy  <= enderecoEscritaMemGyPeloBlocoGy;
			enderecoImagem <= enderecoImagemParaGy;
			carregaImagemReferencia <= carregaImagemReferenciaDoGy;
		end if;
		if (calculaGx = '0' AND calculaGy = '0' AND calculaG = '1') then
			enderecoMemGx <= enderecoMemGPeloBlocoG;
			enderecoMemGy <= enderecoMemGPeloBlocoG;
			enderecoMemG   <= enderecoMemGPeloBlocoG;
		end if;

	end process;

	crregaImagem : carregadorDeImagem
		generic map(DATA_WIDTH             => DATA_WIDTH,
			        ADDR_WIDTH_IMG_NATURAL => ADDR_WIDTH_NATURAL)
		port map(imagemIN  => imagem,
			     escreve   => carregaImagem,
			     carrega   => carregaImagemReferencia,
			     reset     => rst,
			     clk       => clk,
			     endereco  => enderecoImagem,
			     imagemOUT => imagemReferencia,
			     iPronta   => imagemCarregada);
	memGx : memoriaRam
		generic map(DATA_WIDTH         => 8,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk  => clk,
			     addr => enderecoMemGx,
			     data => resultadoGx,
			     we   => escreveMemGx,
			     q    => gx);

	calcGx : blocoCalculaGx
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk             => clk,
			     rst             => rst,
			     imagem          => imagemReferencia,
			     inicia          => calculaGx,
			     enderecoEscrita => enderecoEscritaMemGxPeloBlocoGx,
			     enderecoLeitura => enderecoImagemParaGx,
			     carrega         => carregaImagemReferenciaDoGx,
			     escreve         => escreveMemGx,
			     gxPronto        => gxPronto,
			     resultado       => resultadoGx);

	memGy : memoriaRam
		generic map(DATA_WIDTH         => 8,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk  => clk,
			     addr => enderecoMemGy,
			     data => resultadoGy,
			     we   => escreveMemGy,
			     q    => gy);
	calcGy : blocoCalculaGy
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk             => clk,
			     rst             => rst,
			     imagem          => imagemReferencia,
			     inicia          => calculaGy,
			     enderecoEscrita => enderecoEscritaMemGyPeloBlocoGy,
			     enderecoLeitura => enderecoImagemParaGy,
			     carrega         => carregaImagemReferenciaDoGy,
			     escreve         => escreveMemGy,
			     gyPronto        => gyPronto,
			     resultado       => resultadoGy);

	memG : memoriaRam
		generic map(DATA_WIDTH         => 8,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk  => clk,
			     addr => enderecoMemG,
			     data => resultadoG,
			     we   => escreveMemG,
			     q    => imagemComBordaDetectada);

	calcG : blocoCalculaG
		generic map(DATA_WIDTH         => 8,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk         => clk,
			     rst         => rst,
			     inicia      => calculaG,
			     pixelGx     => gx,
			     pixelGy     => gy,
			     escreveMemG => escreveMemG,
			     endereco    => enderecoMemGPeloBlocoG,
			     gPronto     => gPronto,
			     resultado   => resultadoG);
end architecture comportamente;
