library ieee;
use ieee.std_logic_1164.all;

entity detector is
	generic(DATA_WIDTH         : natural := 8;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		inicia, clk, rst        : in  std_logic;
		imagem                  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		deteccaoPronta          : out std_logic;
		imagemComBordaDetectada : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity detector;

architecture comportamento of detector is
	component bcDetector
		generic(ADDR_WIDTH : natural := 100
		);
		port(
			inicia, clk, rst                              : in  std_logic;
			imagemCarregada, gxPronto, gyPronto, gPronto  : in  std_logic;
			carregaImagem, calculaGx, calculaGy, calculaG : out std_logic;
			bordaDetectada                                : out std_logic
		);
	end component bcDetector;

	component boDetector
	generic(DATA_WIDTH         : natural := 8;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		imagem                                        : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		clk, rst                                      : in  std_logic;
		carregaImagem, calculaGx, calculaGy, calculaG : in std_logic;
		imagemCarregada, gxPronto, gyPronto, gPronto  : out std_logic;
		imagemComBordaDetectada : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
	end component boDetector;
	signal carregaImagem                  : std_logic;
	signal gPronto, gxPronto, gyPronto    : std_logic;
	signal calculaGx, calculaGy, calculaG : std_logic;
	signal imagemCarregada                : std_logic;

begin
	bc : bcDetector
		generic map(ADDR_WIDTH => ADDR_WIDTH_NATURAL)
		port map(inicia          => inicia,
			     clk             => clk,
			     rst             => rst,
			     imagemCarregada => imagemCarregada,
			     gxPronto        => gxPronto,
			     gyPronto        => gyPronto,
			     gPronto         => gPronto,
			     carregaImagem   => carregaImagem,
			     calculaGx       => calculaGx,
			     calculaGy       => calculaGy,
			     calculaG        => calculaG,
			     bordaDetectada  => deteccaoPronta);
	bo : boDetector
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(imagem          => imagem,
			     clk             => clk,
			     rst             => rst,
			     carregaImagem   => carregaImagem,
			     calculaGx       => calculaGx,
			     calculaGy       => calculaGy,
			     calculaG        => calculaG,
			     imagemCarregada => imagemCarregada,
			     gxPronto        => gxPronto,
			     gyPronto        => gyPronto,
			     gPronto         => gPronto,
				  imagemComBordaDetectada=> imagemComBordaDetectada);
end architecture comportamento;
