library ieee;
use ieee.std_logic_1164.all;

entity blocoCalculaG is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, rst         : in  std_logic;
		inicia           : in  std_logic;
		pixelGx, pixelGy : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		escreveMemG      : out std_logic;
		endereco         : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		gPronto          : out std_logic;
		resultado        : out std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end entity blocoCalculaG;

architecture comportamento of blocoCalculaG is
	component boG
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk, calcula     : in  std_logic;
			pixelGx, pixelGy : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			calculoPronto    : out std_logic;
			contador         : in  natural range 0 to ADDR_WIDTH_NATURAL - 1;
			resultado        : out std_logic_vector(DATA_WIDTH - 1 downto 0)
		);
	end component boG;

	component bcG
		generic(ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk, reset            : in  std_logic;
			inicia, calculoPronto : in  std_logic;
			calculaG              : out std_logic;
			escreveMemG           : out std_logic;
			endereco              : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			gPronto               : out std_logic
		);
	end component bcG;
	signal calculaG      : std_logic;
	signal calculoPronto : std_logic;
	signal enderecoIN : natural range 0 to ADDR_WIDTH_NATURAL - 1;

begin
	blocoControle : bcG
		generic map(ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk           => clk,
			     reset         => rst,
			     inicia        => inicia,
			     calculoPronto => calculoPronto,
			     calculaG      => calculaG,
			     escreveMemG   => escreveMemG,
			     endereco      => enderecoIN,
			     gPronto       => gPronto);

	blocoOperativo : boG
		generic map(DATA_WIDTH         => DATA_WIDTH,
			        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
		port map(clk           => clk,
			     calcula       => calculaG,
			     pixelGx       => pixelGx,
			     pixelGy       => pixelGy,
			     calculoPronto => calculoPronto,
			     contador      => enderecoIN,
			     resultado     => resultado); 
		endereco <= enderecoIN;

end architecture comportamento;
