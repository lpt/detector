library ieee;
use ieee.std_logic_1164.all;

entity blocoCalculaGy is
	generic(DATA_WIDTH         : natural := 10;
		    ADDR_WIDTH_NATURAL : natural := 100);
	port(
		clk, rst                         : in  std_logic;
		imagem                           : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
		inicia                           : in  std_logic;
		enderecoEscrita, enderecoLeitura : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
		carrega, escreve                 : out std_logic;
		GyPronto                         : out std_logic;
		resultado                        : out std_logic_vector(7 downto 0));
end entity blocoCalculaGy;

architecture comportamento of blocoCalculaGy is
	component bcGy
		port(
			clk, reset                                      : in  std_logic;
			inicia, carregado, calculado, escritaFinalizada : in  std_logic;
			carregaImagem, calculaGy, escreveNaRam          : out std_logic;
			GyPronto                                        : out std_logic;
			escreve                                         : out std_logic
		);
	end component bcGy;

	component boGy
		generic(DATA_WIDTH         : natural := 10;
			    ADDR_WIDTH_NATURAL : natural := 100);
		port(
			clk                                     : in  std_logic;
			imagem                                  : in  std_logic_vector(DATA_WIDTH - 1 downto 0);
			carregaImagem, calculaGy, escreveNaRam  : in  std_logic;
			carregado, calculado, escritaFinalizada : out std_logic;
			carrega                                 : out std_logic;
			enderecoEscrita, enderecoLeitura        : out natural range 0 to ADDR_WIDTH_NATURAL - 1;
			resultado                               : out std_logic_vector(7 downto 0)
		);
	end component boGy;
	signal calculado         : std_logic;
	signal carregado         : std_logic;
	signal escritaFinalizada : std_logic;
	signal carregaImagem     : std_logic;
	signal calculaGy         : std_logic;
	signal escreveNaRam      : std_logic;

begin
	blocoBc : bcGy
		port map(clk               => clk,
			     reset             => rst,
			     inicia            => inicia,
			     carregado         => carregado,
			     calculado         => calculado,
			     escritaFinalizada => escritaFinalizada,
			     carregaImagem     => carregaImagem,
			     calculaGy         => calculaGy,
			     escreveNaRam      => escreveNaRam,
			     GyPronto          => GyPronto,
			     escreve           => escreve);

blocoBo : boGy
	generic map(DATA_WIDTH         => DATA_WIDTH,
		        ADDR_WIDTH_NATURAL => ADDR_WIDTH_NATURAL)
	port map(clk               => clk,
		     imagem            => imagem,
		     carregaImagem     => carregaImagem,
		     calculaGy         => calculaGy,
		     escreveNaRam      => escreveNaRam,
		     carregado         => carregado,
		     calculado         => calculado,
		     escritaFinalizada => escritaFinalizada,
		     carrega           => carrega,
		     enderecoEscrita   => enderecoEscrita,
		     enderecoLeitura   => enderecoLeitura,
		     resultado         => resultado);
end architecture comportamento;
