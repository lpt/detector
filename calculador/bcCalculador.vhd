library ieee;
use ieee.std_logic_1164.all;

entity bcCalculador is
	port(
		clk, rst                    : in  std_logic;
		inicia                      : in  std_logic;
		gxPronto, gyPronto, gPronto : in  std_logic;
		iniciaGx, iniciaGy, iniciaG : out std_logic
	);
end entity bcCalculador;

architecture comportamento of bcCalculador is
	type estado_type is (ocioso, calculandoGx, calculandoGy, calculandoG);
	signal estado : estado_type := ocioso;

begin
	process(clk, rst)
	begin
		if (rst = '1') then
			estado <= ocioso;
		elsif (rising_edge(clk)) then
			case estado is
				when ocioso =>
					if (inicia = '1') then
						estado <= iniciaGx;
					else
						estado <= ocioso;
					end if;
				when calculandoGx =>
					if (gxPronto = '1') then
						estado <= calculandoGy;
					else
						estado <= calculandoGx;
					end if;
				when calculandoGy =>
					if (gyPronto = '1') then
						estado <= calculandoG;
					else
						estado <= calculandoGy;
					end if;
				when calculandoG =>
					if (gPronto = '1') then
						estado <= ocioso;
					else
						estado <= calculandoG;
					end if;
			end case;
		end if;
	end process;

	process(estado)
	begin
		case estado is
			when ocioso =>
				iniciaGx <= '0';
				iniciaGy <= '0';
				iniciaG  <= '0';
			when calculandoGx =>
				iniciaGx <= '1';
				iniciaGy <= '0';
				iniciaG  <= '0';
			when calculandoGy =>
				iniciaGx <= '0';
				iniciaGy <= '1';
				iniciaG  <= '0';
			when calculandoG =>
				iniciaGx <= '0';
				iniciaGy <= '0';
				iniciaG  <= '1';
		end case;
	end process;

end architecture comportamento;
